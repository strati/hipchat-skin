#!/bin/bash

FOLDER="/Applications/HipChat.app/Contents/Resources"

if [ ! -f $FOLDER/prettify_backup.css ]; then
	mv $FOLDER/prettify.css $FOLDER/prettify_backup.css
	mv $FOLDER/prettify.js  $FOLDER/prettify_backup.js
else
	rm $FOLDER/prettify.css
	rm $FOLDER/prettify.js
fi

ln -s `pwd`/prettify.js  $FOLDER/prettify.js
ln -s `pwd`/prettify.css $FOLDER/prettify.css

echo "prettify files are linked, and backuped."
